<?php
/**
 * User: krona
 * Date: 11/14/14
 * Time: 6:42 PM
 */

namespace Arilas\ORM\Mapping;

/**
 * Class Virtual
 * @package Arilas\ORM\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Virtual
{
    public $query;
}