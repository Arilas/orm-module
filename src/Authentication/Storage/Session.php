<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/14/13
 * Time: 6:37 PM
 */

namespace Arilas\ORM\Authentication\Storage;

use Zend\Authentication\Storage\Session as BaseSession;

class Session extends BaseSession
{
    public function getContainer()
    {
        return $this->session;
    }

    public function rememberMe($time)
    {
        $this->session->getManager()->rememberMe($time);
    }
}
