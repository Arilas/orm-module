<?php
/**
 * User: granted
 * Date: 12/23/14
 * Time: 2:52 PM
 */

namespace Arilas\ORM\Query;


use Arilas\ORM\EntityManager;
use Arilas\ORM\Exception\RuntimeException;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\ORM\Query\ParameterTypeInferer;
use Krona\Common\Object\ObjectInterface;

class Query
{
    /**
     * Hydrates an object graph. This is the default behavior.
     */
    const HYDRATE_OBJECT = 1;

    /**
     * Hydrates an array graph.
     */
    const HYDRATE_ARRAY = 2;

    /**
     * Hydrates a flat, rectangular result set with scalar values.
     */
    const HYDRATE_SCALAR = 3;

    /**
     * Hydrates a single scalar value.
     */
    const HYDRATE_SINGLE_SCALAR = 4;

    /**
     * Very simple object hydrator (optimized for performance).
     */
    const HYDRATE_SIMPLEOBJECT = 5;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var int
     */
    protected $hydrationMode = self::HYDRATE_OBJECT;

    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @var string
     */
    protected $sql;

    /**
     * @var string
     */
    protected $className;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param int $hydrationMode
     * @return array
     * @throws RuntimeException
     */
    public function getResult($hydrationMode = self::HYDRATE_OBJECT)
    {
        $stmt = $this->execute([], $hydrationMode);

        return $this->hydrateAll($stmt);
    }

    public function execute($parameters = [], $hydrationMode = null)
    {
        if ($hydrationMode !== null) {
            $this->setHydrationMode($hydrationMode);
        }

        if (!empty($parameters)) {
            $this->setParameters(array_replace($this->getParameters(), $parameters));
        }

        return $this->doExecute();
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    protected function doExecute()
    {
        $parameters = array();
        $types = array();

        foreach ($this->getParameters() as $name => $parameter) {
            $value = $this->processParameterValue($parameter);
            $type = ParameterTypeInferer::inferType($value);

            $parameters[$name] = $value;
            $types[$name] = $type;
        }

        if ($parameters && is_int(key($parameters))) {
            ksort($parameters);
            ksort($types);

            $parameters = array_values($parameters);
            $types = array_values($types);
        }

        return $this->em->getConnection()->executeQuery(
            $this->sql,
            $parameters,
            $types
        );
    }

    public function processParameterValue($value)
    {
        if (is_scalar($value)) {
            return $value;
        }

        if (is_array($value)) {
            foreach ($value as $key => $paramValue) {
                $paramValue = $this->processParameterValue($paramValue);
                $value[$key] = is_array($paramValue) ? reset($paramValue) : $paramValue;
            }

            return $value;
        }

        if (is_object($value) && $value instanceof ObjectInterface) {
            $value = $value->getId();
        }

        return $value;
    }

    /**
     * @param Statement $statement
     * @return array
     * @throws RuntimeException
     */
    protected function hydrateAll(Statement $statement)
    {
        $result = [];
        switch ($this->getHydrationMode()) {
            case self::HYDRATE_OBJECT:
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    $entity = $this->em->getRepository($this->className)->getUnitOfWork()->hydrate($row);
                    $result[$entity->getId()] = $entity;
                }
                break;
            default:
                throw new RuntimeException('Not Supported Hydration mode');
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getHydrationMode()
    {
        return $this->hydrationMode;
    }

    /**
     * @param int $hydrationMode
     * @return $this
     */
    public function setHydrationMode($hydrationMode)
    {
        $this->hydrationMode = $hydrationMode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param string $sql
     * @return $this
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param string $className
     * @return $this
     */
    public function setClassName($className)
    {
        $this->className = $className;
        return $this;
    }
}