<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/24/13
 * Time: 3:08 PM
 */

namespace Arilas\ORM\Service;

use Arilas\ORM\Exception\RuntimeException;
use Doctrine\DBAL\Connection;
use DoctrineORMModule\Service\DBALConnectionFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConnectionFactory
{
    /** @var  array */
    protected $config;
    /** @var array */
    protected $pull = array();

    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->fetchConfig();
    }

    protected function fetchConfig()
    {
        if (is_null($this->config)) {
            $config = $this->serviceLocator->get('Config');
            $this->config = $config['doctrine']['connection'];
        }
    }

    /**
     * Create Db Connection with name
     *
     * @param  string                          $requestedName
     * @throws \Arilas\ORM\Exception\RuntimeException
     * @return Connection
     */
    public function getConnection($requestedName)
    {
        if ($this->canCreateConnection($requestedName)) {
            if (!isset($this->pull[$requestedName])) {
                $this->createConnection($requestedName);
            }

            return $this->pull[$requestedName];
        } else {
            throw new RuntimeException(
                'Connection with name: ' . $requestedName . ' could not created, check your Configuration.'
            );
        }
    }

    /**
     * Determine if we can create a Connection with name
     *
     * @param  string $requestedName
     * @return bool
     */
    protected function canCreateConnection($requestedName)
    {
        return isset($this->config[$requestedName]) && !empty($this->config[$requestedName]);
    }

    protected function createConnection($requestedName)
    {
        $factory = new DBALConnectionFactory($requestedName);
        $this->pull[$requestedName] = $factory->createService($this->serviceLocator);
    }
}
