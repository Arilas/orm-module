<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 2:05
 */

namespace Arilas\ORM\Entity;

use Krona\Common\Object\ObjectInterface;

/**
 * An Interface that Entity must implement
 * Interface EntityInterface
 * @package Arilas\ORM\Entity
 */
interface EntityInterface extends ObjectInterface
{
} 