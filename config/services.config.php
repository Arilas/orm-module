<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 4:16 PM
 */
return [
    'factories' => [
        \Arilas\ORM\EntityManager::class => new Arilas\ORM\Service\Factory(),
        'arilas.orm.authentication_service' => new Arilas\ORM\Authentication\Service(),
    ],
    'aliases' => [
        'arilas.orm.entity_manager' => \Arilas\ORM\EntityManager::class,
    ],
    'invokables' => [
        \Arilas\ORM\Mvc\Param\EntityParamConverter::class => \Arilas\ORM\Mvc\Param\EntityParamConverter::class,
        \Arilas\ORM\Mvc\Param\EntityTypeParamConverter::class => \Arilas\ORM\Mvc\Param\EntityTypeParamConverter::class,
        'EntityStrategy' => \Arilas\ORM\View\Strategy\EntityStrategy::class,
    ]
];
