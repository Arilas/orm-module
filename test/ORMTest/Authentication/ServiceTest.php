<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/25/13
 * Time: 6:20 PM
 */

namespace Arilas\ORMTest\Authentication;

use Arilas\ORM\Repository\AbstractRepository;
use Arilas\ORMTest\Bootstrap;
use Doctrine\ORM\EntityManager;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\ServiceManager\ServiceManager;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ServiceManager */
    protected static $sm;
    /** @var  \Arilas\ORM\EntityManager */
    protected static $orm;
    /** @var  EntityManager */
    protected static $em;
    /** @var  AbstractRepository */
    protected static $repository;

    public static function setUpBeforeClass()
    {
        self::$sm = Bootstrap::getServiceManager();
        self::$orm = self::$sm->get('arilas.orm.entity_manager');
        self::$em = self::$sm->get('doctrine.entitymanager.orm_default');

        self::$repository = static::$orm->getRepository('Arilas\ORMTest\Test\Test');

        $tool = new \Doctrine\ORM\Tools\SchemaTool(static::$em);
        $classes = array(
            static::$repository->getClassMetadata()
        );
        $tool->updateSchema($classes);
    }

    public function testGetAuthInstance()
    {
        /** @var AuthenticationService $authService */
        $authService = static::$sm->get('arilas.orm.authentication_service');
        $this->assertInstanceOf(AuthenticationServiceInterface::class, $authService);
        $this->assertInstanceOf('Arilas\ORM\Authentication\Storage\Session', $authService->getStorage());
    }
}
