<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 3:47
 */

namespace Arilas\ORMTest;


use Arilas\ORM\EntityManager;
use Arilas\ORM\Repository\AbstractRepository;
use Arilas\ORMTest\Test\Test;
use Arilas\ORMTest\Test\TestParent;
use Doctrine\ORM\Tools\SchemaTool;
use Zend\ServiceManager\ServiceManager;

trait AbstractTest
{
    /** @var  ServiceManager */
    protected static $sm;
    /** @var  EntityManager */
    protected static $orm;
    /** @var  \Doctrine\ORM\EntityManager */
    protected static $em;
    /** @var  AbstractRepository */
    protected static $testRepository;
    /** @var  AbstractRepository */
    protected static $testParentRepository;

    public static function setUpBeforeClass()
    {
        self::$sm = Bootstrap::getServiceManager();
        self::$orm = self::$sm->get('arilas.orm.entity_manager');
        self::$em = self::$sm->get('doctrine.entitymanager.orm_default');
        self::$testParentRepository = static::$orm->getRepository(TestParent::class);
        self::$testRepository = static::$orm->getRepository(Test::class);

        $tool = new SchemaTool(static::$em);
        $classes = [
            static::$testRepository->getClassMetadata(),
            static::$testParentRepository->getClassMetadata(),
        ];
        $tool->updateSchema($classes);
    }

    public static function tearDownAfterClass()
    {
        $tool = new SchemaTool(self::$em);
        $tool->dropSchema(
            [
                static::$testRepository->getClassMetadata(),
                static::$testParentRepository->getClassMetadata(),
            ]
        );
        static::$orm->clear(Test::class);
        static::$orm->clear();
    }
} 