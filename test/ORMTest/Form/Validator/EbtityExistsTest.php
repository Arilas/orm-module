<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 11/22/13
 * Time: 3:27 PM
 */

namespace Arilas\ORMTest\Form\Validator;

use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Test\Test;
use Krona\CommonModule\Form\Validator\ObjectExists;
use PHPUnit_Framework_TestCase;

class EntityExistsTest extends PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testExist()
    {
        $testEntity = new Test();
        $testEntity->setValue("test");
        $testEntity->id = 1;
        self::$orm->commit($testEntity);

        $validator = new ObjectExists(
            array(
                'objectManager' => self::$orm,
                'objectClassName' => Test::class
        ));
        $this->assertTrue($validator->isValid(1));

        self::$orm->remove($testEntity);
    }

    public function testNotExist()
    {
        $validator = new ObjectExists(
            array(
                'objectManager' => self::$orm,
                'objectClassName' => Test::class
        ));
        $this->assertFalse($validator->isValid(1));
    }
}
