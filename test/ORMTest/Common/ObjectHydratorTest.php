<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 1:43
 */
namespace Arilas\ORMTest\Common;

use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Test\Test;
use PHPUnit_Framework_TestCase;

class ObjectHydratorTest extends PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testHydrate()
    {
        $data = [
            'id' => 1,
            'value' => 'test',
            'array' => "[]",
            'datetime' => '2014-08-17 06:45:15',
        ];

        $object = static::$orm->getObjectHydrator()->hydrate(
            $data,
            new Test()
        );

        $this->assertInstanceOf(Test::class, $object);
        $this->assertSame($data['id'], $object->getId());
        $this->assertSame($data['value'], $object->getValue());

        $newData = static::$orm->getObjectHydrator()->extract($object);

        $this->assertEquals($data, $newData);
    }
}
 