<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 1:23
 */

namespace Arilas\ORMTest\Test;


use Arilas\ORM\EntityManager;
use Arilas\ORM\Mvc\Controller\AbstractResource;
use Krona\CommonModule\Reflection\Util\Reflector;

class TestResource extends AbstractResource
{
    public function testAction(Test $test, Reflector $reflector, $id, array $data = null)
    {
        return [
            'data' => $data,
            'test' => $test,
            'reflector' => $reflector,
            'id' => $id,
        ];
    }

    public function create(EntityManager $entityManager, array $data = null)
    {
        return [
            'entityManager' => $entityManager,
            'data' => $data,
        ];
    }
}