<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 12.02.14
 * Time: 4:16
 */
//Remove reposes
//exec("find . | grep .git | xargs rm -rf");
if (!file_exists('builds')) {
    mkdir('builds');
}
$buildNumber = getenv('DRONE_BUILD_NUMBER');
if (is_string($buildNumber) || is_numeric($buildNumber)) {
    $fileName = 'builds/arilas-orm-module_' . $buildNumber . '.phar';
} else {
    $fileName = 'builds/arilas-orm-module_SNAPSHOT.phar';
}

$phar = new Phar($fileName, 0, $fileName);
//Add files to Phar
$phar->buildFromDirectory(dirname(__FILE__), '/src/');
$phar->buildFromDirectory(dirname(__FILE__), '/config/');
$phar->addFile('Module.php');
$phar->addFile('LICENSE');
$phar->addFile('README.md');
$phar->convertToData(Phar::ZIP);
unlink($fileName);
